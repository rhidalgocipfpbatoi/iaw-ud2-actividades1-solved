<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>IAW-UD2-A2</title>
</head>
<body>
<?php

/**
 * Modifica el programa anterior para que muestre tu dirección y tu número de teléfono.
 * Cada dato se debe mostrar en una línea diferente
 */

    echo "<p>Roberto Hidalgo Martínez</p>";
    echo "<p>C/ Alicante 80</p>";
    echo "<p>657 65 45 54</p>";

?>
</body>
</html>

