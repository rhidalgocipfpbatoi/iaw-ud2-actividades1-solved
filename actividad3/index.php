<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>IAW-UD2-A3</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
<?php

/**
 * Escribe un programa que muestre por pantalla 10 palabras en inglés junto a su correspondiente
 * traducción al castellano. Las palabras deben estar distribuidas en dos columnas. Utiliza la etiqueta
 * <table> de HTML.
 */

echo "<table style=\"text-align: center;\">
        <tr>
            <th>Inglés</th>
            <th>Español</th>
        </tr>
        <tr>
            <td>Refactoring</td>
            <td>Refactorizar</td>
        </tr>
        <tr>
            <td>Repository</td>
            <td>Repositorio</td>
        </tr>
        <tr>
            <td>Programing Language</td>
            <td>Lenguaje de programación</td>
        </tr>
        <tr>
            <td>Backup</td>
            <td>Copia de Seguridad</td>
        </tr>
        <tr>
            <td>Bug</td>
            <td>Error</td>
        </tr>
        <tr>
            <td>Code</td>
            <td>Codigo</td>
        </tr>
        <tr>
            <td>Data</td>
            <td>Datos</td>
        </tr>
        <tr>
            <td>Desktop</td>
            <td>Escritorio</td>
        </tr>
        <tr>
            <td>Developer</td>
            <td>Programador</td>
        </tr>
     </table>";
?>
</table>
</body>
</html>