<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>IAW-UD2-A6</title>
</head>
<body>
<?php

/**
 * Escribe un programa que declare 5 variables `$preferencia1`, `$preferencia2`,.. `$preferencia5`, almacena en cada
 * una de ellas Èl nombre de un IDE para php y genere una página web con una lista ordenada
 */

$preferencia1="PHPStorm";
$preferencia2="VisualStudio Code";
$preferencia3="Eclipse for developers" ;
$preferencia4="Netbeans";
$preferencia5="Atom";

echo "<ul>
          <li>$preferencia1</li>
          <li>$preferencia2</li>
          <li>$preferencia3</li>
          <li>$preferencia4</li>
          <li>$preferencia5</li>
     <ul>";
?>
</body>
</html>