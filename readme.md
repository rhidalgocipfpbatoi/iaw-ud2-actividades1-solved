# Conociendo el Lenguaje Php (I)
####  Inserción de código en páginas web, variables, errores y mensajes de Error

#### Activitad 1

Convierte la página estática `index.html` facilitada, en un script php que muestre tu nombre y apellidos.
Utiliza código php para generar la salida

#### Activitad 2

Modifica el programa anterior para que muestre tu dirección y tu número de teléfono. Cada dato
se debe mostrar en una línea diferente. `<p></p>`

#### Activitad 3

Escribe un programa que muestre por pantalla 10 palabras en inglés junto a su correspondiente
traducción al castellano. Las palabras deben estar distribuidas en dos columnas. Utiliza la etiqueta
`<table>` de HTML.

| Inglés        | Español    |
| :-------------: |:-------------:|
| Refactoring   | Refactorizar |
| Repository          | Repositorio      |
| Programing Language      | Lenguaje de Programación      |

#### Activitad 4

Escribe un programa que muestre tu horario de clase mediante una tabla. 
Aunque se puede hacer integramente con HTML, utiliza las directivas `<?= ... ?>` para mostrar cada línea.

#### Activitad 5

Escribe un programa que cree la variable `$nombre`, le asigne tu nombre completo y muestre su valor por pantalla de forma
que el resultado sea el mismo que el del ejercicio 1.

#### Activitad 6

Escribe un programa que declare 5 variables `$preferencia1`, `$preferencia2`,.. `$preferencia5`, almacena en cada 
una de ellas Èl nombre de un IDE para php y genere una página web con una lista ordenada. `<ul><li></li></ul>`

1. PHPStorm
2. VisualStudio Code
3. Eclipse for developers
4. NetBeans
5. Atom

#### Activitad 7
1. Muestra por pantalla una pirámide hecha con asteriscos de base 9
2. Muéstrala vacia
3. Muéstrala invertida

#### Activitad 8

A partir del script php facilitado:

1. Establece la directiva correspondiente (en el propio script) para que el intérprete muestre todos los mensajes de **error,
   advertencia y alerta**.
2. Escribe un comentario que se visualice en el html resultante sobre el tipo de mensaje que se está produciendo en cada caso. 
3. Refactoriza el código para que no se produzcan los errores. 
    **Nota** Comenta aquellos que no se necesiten para el correcto funcionamiento del programa
4. Describe el significado de cada error. ¿Quñe opción activarias para que solo se mostrase ese tipo de error?

Lee el siguiente enlace [Php.net Error Reporting](https://www.php.net/manual/es/function.error-reporting.php);
